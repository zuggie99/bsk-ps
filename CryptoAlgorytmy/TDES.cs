﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Text;

namespace CryptoAlgorytmy
{
	public static class TDES
	{
		public static string Zaszyfruj(string tekst, string klucz1, string klucz2)
		{
			byte[] result = UTF8Encoding.UTF8.GetBytes(tekst);			
			result = encrypt(result, klucz1);
			result = decrypt(result, klucz2);
			result = encrypt(result, klucz1);
			
			return Convert.ToBase64String(result, 0, result.Length);		
		}

		public static string Odszyfruj(string tekst, string klucz1, string klucz2)
		{
			byte[] result = Convert.FromBase64String(tekst);			
			result = decrypt(result, klucz1);
			result = encrypt(result, klucz2);
			result = decrypt(result, klucz1);

			return UTF8Encoding.UTF8.GetString(result);
		}

		private static byte[] encrypt(byte[] tekst, string klucz)
		{
			DESCryptoServiceProvider des = new DESCryptoServiceProvider();
			des.Key = UTF8Encoding.UTF8.GetBytes(klucz);
			des.Mode = CipherMode.ECB;
			des.Padding = PaddingMode.Zeros;

			byte[] result = des.CreateEncryptor().TransformFinalBlock(tekst, 0, tekst.Length);
			des.Clear();			 

			return result;
		}

		private static byte[] decrypt(byte[] tekst, string klucz)
		{
			
			DESCryptoServiceProvider des = new DESCryptoServiceProvider();
			des.Key = UTF8Encoding.UTF8.GetBytes(klucz);			
			des.Mode = CipherMode.ECB;
			des.Padding = PaddingMode.Zeros;

			byte[] result = des.CreateDecryptor().TransformFinalBlock(tekst, 0, tekst.Length);
			des.Clear();
			
			return result;
		}
	}
}
