﻿using System;
using System.Text;

namespace CryptoAlgorytmy
{
	/// <summary>	Szyfr cezara. </summary>
	///
	/// <remarks>	Adam, 3/28/2014. </remarks>
	public static class SzyfrCezara
	{
		private static int DWADO16 = 65536;

		/// <summary>	Zaszyfruj. Szyfruje podany string na podstawie podanego przesunięcia. </summary>
		///
		/// <remarks>	Adam, 3/28/2014. </remarks>
		///
		/// <param name="tekst">	Tekst do zaszyfrowania. </param>
		/// <param name="key">  	int k: przesunięcie o ilość znaków </param>
		///
		/// <returns>	String. Zaszyfrowany tekst. </returns>
		public static String Zaszyfruj(String tekst, int k)
		{
			StringBuilder sb = new StringBuilder();

			// przesunięcie, urzywamy znakow 16-bitowych
			foreach (char c in tekst)
				sb.Append(Convert.ToChar((c + k) % DWADO16));

			return sb.ToString();
		}

		/// <summary>	Odszyfruj. Odszyfruje podany string na podstawie podanego przesunięcia. </summary>
		///
		/// <remarks>	Adam, 3/28/2014. </remarks>
		///
		/// <param name="tekst">	Tekst do odszyfrowania. </param>
		/// <param name="key">  	int k: przesunięcie o ilość znaków </param>
		///
		/// <returns>	String. Odszyfrowany tekst. </returns>
		public static String Odszyfruj(String tekst, int k)
		{
			StringBuilder sb = new StringBuilder();

			foreach (char c in tekst)
				sb.Append(Convert.ToChar((c + (DWADO16 - k)) % DWADO16));

			return sb.ToString();
		}
	}
}
