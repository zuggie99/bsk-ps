﻿using System;
using System.Text;

namespace CryptoAlgorytmy
{
	/// <summary>	Kryptosystem przestawieniowy. </summary>
	///
	/// <remarks>	Adam, 3/28/2014. </remarks>
	public static class KryptosystemPrzestawieniowy
	{
		/// <summary>	Zaszyfruj. Szyfruje podany string na podstawie klucza
		/// 			składającego się z numerów kolumn w macierzy. </summary>
		///
		/// <remarks>	Adam, 3/28/2014. </remarks>
		///
		/// <param name="tekst">	Tekst do zaszyfrowania. </param>
		/// <param name="key">  	Klucz (numery kolumn macierzy) </param>
		///
		/// <returns>	String. Zaszyfrowany tekst. </returns>
		public static String Zaszyfruj(String tekst, String key)
		{
			string[] klucz = key.Trim().Split();
			StringBuilder sb = new StringBuilder();
			int idx = 0;

			while (sb.Length < tekst.Length)
			{
				for (int y = 0; y < klucz.Length; y++)
				{
					// index nastepnego znaku ze stringa do zapisania
					int k = idx + Convert.ToInt32(klucz[y]) - 1;
					// w ostatnim rzędzie moze wypasc poza ostatnim zgodnie z kluczem, ale te ignorujemy 
					if (k < tekst.Length) sb.Append(tekst[k]);
				}
				idx += klucz.Length;
			}

			return sb.ToString();
		}

		/// <summary>	Odszyfruj. Odszyfruje podany string na podstawie klucza
		/// 			składającego się z numerów kolumn w macierzy. </summary>
		///
		/// <remarks>	Adam, 3/28/2014. </remarks>
		///
		/// <param name="tekst">	Tekst do odszyfrowania. </param>
		/// <param name="key">  	Klucz (numery kolumn macierzy) </param>
		///
		/// <returns>	String. Odszyfrowany tekst. </returns>
		public static String Odszyfruj(String tekst, String key)
		{
			string[] klucz = key.Trim().Split();
			char[] s = new char[tekst.Length];
			int idx = 0;

			while (idx < tekst.Length)
			{
				int i = idx;
				for (int y = 0; y < klucz.Length; y++)
				{
					int k = i + Convert.ToInt32(klucz[y]) - 1;
					// ignorujemy znaki ktore w ostatnim wierszu wypadalyby poza ostatnim znakiem tekstu
					// ale uporzadkowanie waznych znakow zalezy od klucza
					if (idx < tekst.Length && k < tekst.Length)
					{
						s[k] = tekst[idx];
						idx++;
					}
				}
			}

			return new String(s);
		}
	}
}
