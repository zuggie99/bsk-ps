﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoAlgorytmy
{
	/// <summary>	Rail fence algorytm. </summary>
	///
	/// <remarks>	Adam, 3/28/2014. </remarks>
	public static class RailFence
	{
		/// <summary>	Zaszyfruj. Szyfruje podany string na podstawie podanej liczby poziomów. </summary>
		///
		/// <remarks>	Adam, 3/28/2014. </remarks>
		///
		/// <param name="tekst">	Tekst do zaszyfrowania. </param>
		/// <param name="key">  	int n: liczba pozimów w płotku </param>
		///
		/// <returns>	String. Zaszyfrowany tekst. </returns>
		public static String Zaszyfruj(String tekst, int n)
		{
			StringBuilder sb = new StringBuilder();
			List<StringBuilder> poziomy = new List<StringBuilder>();

			// dodajemy dla kazdego pozimu
			for (int i = 0; i < n; i++) poziomy.Add(new StringBuilder());

			int idx = 0;
			int inc = 1;

			foreach (char c in tekst)
			{
				// jak jesteśmy na dole lub na górze płotka zawrcamy
				if (idx + inc == n || idx + inc == -1) inc *= -1;

				// wpisujemy na obecnym poziomie 
				poziomy[idx].Append(c);
				// +/-1, do góry lub do dołu
				idx += inc;
			}

			// budujemy końcowy string
			foreach (StringBuilder s in poziomy) sb.Append(s);

			return sb.ToString();
		}

		/// <summary>	Zaszyfruj. Odszyfruje podany string na podstawie podanej liczby poziomów. </summary>
		///
		/// <remarks>	Adam, 3/28/2014. </remarks>
		///
		/// <param name="tekst">	Tekst do odszyfrowania. </param>
		/// <param name="key">  	int n: liczba pozimów w płotku </param>
		///
		/// <returns>	String. Odszyfrowany tekst. </returns>
		public static String Odszyfruj(String tekst, int n)
		{
			StringBuilder sb = new StringBuilder();
			List<List<int>> poziomy = new List<List<int>>();
			char[] s = new char[tekst.Length];

			// dla kazdego poziomu dodajemy nową listę
			for (int i = 0; i < n; i++) poziomy.Add(new List<int>());

			// zapisujamy pozycje znaków do poziomow w liscie
			for (int i = 0, idx = 0, inc = 1; i < tekst.Length; i++)
			{
				if (idx + inc == n || idx + inc == -1) inc *= -1;

				poziomy[idx].Add(i);
				idx += inc;
			}

			// Odszyfrowujemy znaki ze stringu na podst. pozycji zapisanych w liscie
			for (int i = 0, y = 0; i < n; i++)
			{
				for (int j = 0; j < poziomy[i].Count; j++, y++)
					s[poziomy[i][j]] = tekst[y];
			}

			return new String(s);
		}
	}
}
