﻿namespace Crypto1Stringi
{
	partial class frmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtNieszyfrowany = new System.Windows.Forms.TextBox();
			this.txtZaszyfrowany = new System.Windows.Forms.TextBox();
			this.txtOdszyfrowany = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label7 = new System.Windows.Forms.Label();
			this.txtTDESkey1 = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.rdoRail = new System.Windows.Forms.RadioButton();
			this.nudN = new System.Windows.Forms.NumericUpDown();
			this.txtKlucz = new System.Windows.Forms.TextBox();
			this.rdoMacierz = new System.Windows.Forms.RadioButton();
			this.rdoTDES = new System.Windows.Forms.RadioButton();
			this.cmdOdszyfruj = new System.Windows.Forms.Button();
			this.cmdSzyfruj = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.nudK = new System.Windows.Forms.NumericUpDown();
			this.rdoCezar = new System.Windows.Forms.RadioButton();
			this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			this.rectangleShape4 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
			this.rectangleShape3 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
			this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
			this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.txtTDESkey2 = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudK)).BeginInit();
			this.SuspendLayout();
			// 
			// txtNieszyfrowany
			// 
			this.txtNieszyfrowany.Location = new System.Drawing.Point(12, 136);
			this.txtNieszyfrowany.Multiline = true;
			this.txtNieszyfrowany.Name = "txtNieszyfrowany";
			this.txtNieszyfrowany.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtNieszyfrowany.Size = new System.Drawing.Size(760, 100);
			this.txtNieszyfrowany.TabIndex = 0;
			// 
			// txtZaszyfrowany
			// 
			this.txtZaszyfrowany.BackColor = System.Drawing.SystemColors.Window;
			this.txtZaszyfrowany.Location = new System.Drawing.Point(11, 256);
			this.txtZaszyfrowany.Multiline = true;
			this.txtZaszyfrowany.Name = "txtZaszyfrowany";
			this.txtZaszyfrowany.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtZaszyfrowany.Size = new System.Drawing.Size(760, 100);
			this.txtZaszyfrowany.TabIndex = 1;
			// 
			// txtOdszyfrowany
			// 
			this.txtOdszyfrowany.Location = new System.Drawing.Point(11, 375);
			this.txtOdszyfrowany.Multiline = true;
			this.txtOdszyfrowany.Name = "txtOdszyfrowany";
			this.txtOdszyfrowany.ReadOnly = true;
			this.txtOdszyfrowany.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtOdszyfrowany.Size = new System.Drawing.Size(760, 100);
			this.txtOdszyfrowany.TabIndex = 2;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.txtTDESkey2);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.txtTDESkey1);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.rdoRail);
			this.groupBox1.Controls.Add(this.nudN);
			this.groupBox1.Controls.Add(this.txtKlucz);
			this.groupBox1.Controls.Add(this.rdoMacierz);
			this.groupBox1.Controls.Add(this.rdoTDES);
			this.groupBox1.Controls.Add(this.cmdOdszyfruj);
			this.groupBox1.Controls.Add(this.cmdSzyfruj);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.nudK);
			this.groupBox1.Controls.Add(this.rdoCezar);
			this.groupBox1.Controls.Add(this.shapeContainer1);
			this.groupBox1.Location = new System.Drawing.Point(11, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(760, 114);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Typ szyfrowania";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(484, 55);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(39, 13);
			this.label7.TabIndex = 19;
			this.label7.Text = "Klucz I";
			// 
			// txtTDESkey1
			// 
			this.txtTDESkey1.Location = new System.Drawing.Point(373, 53);
			this.txtTDESkey1.Name = "txtTDESkey1";
			this.txtTDESkey1.Size = new System.Drawing.Size(105, 20);
			this.txtTDESkey1.TabIndex = 18;
			this.txtTDESkey1.Text = "5F10BEFC";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(17, 76);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(73, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "n - głębokość";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(135, 77);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(33, 13);
			this.label5.TabIndex = 12;
			this.label5.Text = "Klucz";
			// 
			// rdoRail
			// 
			this.rdoRail.AutoSize = true;
			this.rdoRail.Checked = true;
			this.rdoRail.Location = new System.Drawing.Point(20, 31);
			this.rdoRail.Name = "rdoRail";
			this.rdoRail.Size = new System.Drawing.Size(73, 17);
			this.rdoRail.TabIndex = 0;
			this.rdoRail.TabStop = true;
			this.rdoRail.Text = "Rail fence";
			this.rdoRail.UseVisualStyleBackColor = true;
			// 
			// nudN
			// 
			this.nudN.Location = new System.Drawing.Point(20, 54);
			this.nudN.Name = "nudN";
			this.nudN.Size = new System.Drawing.Size(54, 20);
			this.nudN.TabIndex = 9;
			this.nudN.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
			// 
			// txtKlucz
			// 
			this.txtKlucz.Location = new System.Drawing.Point(138, 52);
			this.txtKlucz.Name = "txtKlucz";
			this.txtKlucz.Size = new System.Drawing.Size(95, 20);
			this.txtKlucz.TabIndex = 11;
			// 
			// rdoMacierz
			// 
			this.rdoMacierz.AutoSize = true;
			this.rdoMacierz.Location = new System.Drawing.Point(138, 31);
			this.rdoMacierz.Name = "rdoMacierz";
			this.rdoMacierz.Size = new System.Drawing.Size(82, 17);
			this.rdoMacierz.TabIndex = 1;
			this.rdoMacierz.Text = "Macierzowe";
			this.rdoMacierz.UseVisualStyleBackColor = true;
			// 
			// rdoTDES
			// 
			this.rdoTDES.AutoSize = true;
			this.rdoTDES.Location = new System.Drawing.Point(373, 31);
			this.rdoTDES.Name = "rdoTDES";
			this.rdoTDES.Size = new System.Drawing.Size(54, 17);
			this.rdoTDES.TabIndex = 16;
			this.rdoTDES.Text = "TDES";
			this.rdoTDES.UseVisualStyleBackColor = true;
			// 
			// cmdOdszyfruj
			// 
			this.cmdOdszyfruj.Location = new System.Drawing.Point(652, 57);
			this.cmdOdszyfruj.Name = "cmdOdszyfruj";
			this.cmdOdszyfruj.Size = new System.Drawing.Size(89, 23);
			this.cmdOdszyfruj.TabIndex = 15;
			this.cmdOdszyfruj.Text = "Odszyfruj";
			this.cmdOdszyfruj.UseVisualStyleBackColor = true;
			this.cmdOdszyfruj.Click += new System.EventHandler(this.cmdOdszyfruj_Click);
			// 
			// cmdSzyfruj
			// 
			this.cmdSzyfruj.Location = new System.Drawing.Point(652, 23);
			this.cmdSzyfruj.Name = "cmdSzyfruj";
			this.cmdSzyfruj.Size = new System.Drawing.Size(89, 23);
			this.cmdSzyfruj.TabIndex = 7;
			this.cmdSzyfruj.Text = "Zaszyfruj";
			this.cmdSzyfruj.UseVisualStyleBackColor = true;
			this.cmdSzyfruj.Click += new System.EventHandler(this.cmdSzyfruj_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(256, 76);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(48, 13);
			this.label6.TabIndex = 14;
			this.label6.Text = "k - offset";
			// 
			// nudK
			// 
			this.nudK.Location = new System.Drawing.Point(259, 53);
			this.nudK.Name = "nudK";
			this.nudK.Size = new System.Drawing.Size(54, 20);
			this.nudK.TabIndex = 13;
			this.nudK.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
			// 
			// rdoCezar
			// 
			this.rdoCezar.AutoSize = true;
			this.rdoCezar.Location = new System.Drawing.Point(259, 31);
			this.rdoCezar.Name = "rdoCezar";
			this.rdoCezar.Size = new System.Drawing.Size(58, 17);
			this.rdoCezar.TabIndex = 2;
			this.rdoCezar.Text = "Cezara";
			this.rdoCezar.UseVisualStyleBackColor = true;
			// 
			// shapeContainer1
			// 
			this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
			this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
			this.shapeContainer1.Name = "shapeContainer1";
			this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape4,
            this.rectangleShape3,
            this.rectangleShape2,
            this.rectangleShape1});
			this.shapeContainer1.Size = new System.Drawing.Size(754, 95);
			this.shapeContainer1.TabIndex = 17;
			this.shapeContainer1.TabStop = false;
			// 
			// rectangleShape4
			// 
			this.rectangleShape4.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
			this.rectangleShape4.CornerRadius = 6;
			this.rectangleShape4.Location = new System.Drawing.Point(358, 8);
			this.rectangleShape4.Name = "rectangleShape4";
			this.rectangleShape4.Size = new System.Drawing.Size(185, 82);
			// 
			// rectangleShape3
			// 
			this.rectangleShape3.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
			this.rectangleShape3.CornerRadius = 6;
			this.rectangleShape3.Location = new System.Drawing.Point(240, 8);
			this.rectangleShape3.Name = "rectangleShape3";
			this.rectangleShape3.Size = new System.Drawing.Size(114, 82);
			// 
			// rectangleShape2
			// 
			this.rectangleShape2.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
			this.rectangleShape2.CornerRadius = 6;
			this.rectangleShape2.Location = new System.Drawing.Point(122, 8);
			this.rectangleShape2.Name = "rectangleShape2";
			this.rectangleShape2.Size = new System.Drawing.Size(114, 82);
			// 
			// rectangleShape1
			// 
			this.rectangleShape1.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
			this.rectangleShape1.CornerRadius = 6;
			this.rectangleShape1.Location = new System.Drawing.Point(4, 8);
			this.rectangleShape1.Name = "rectangleShape1";
			this.rectangleShape1.Size = new System.Drawing.Size(114, 82);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 120);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Tekst jawny";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 240);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "Tekst zaszyfrowany";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 359);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(101, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Tekst odszyfrowany";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(484, 82);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(42, 13);
			this.label8.TabIndex = 21;
			this.label8.Text = "Klucz II";
			// 
			// txtTDESkey2
			// 
			this.txtTDESkey2.Location = new System.Drawing.Point(373, 79);
			this.txtTDESkey2.Name = "txtTDESkey2";
			this.txtTDESkey2.Size = new System.Drawing.Size(105, 20);
			this.txtTDESkey2.TabIndex = 20;
			this.txtTDESkey2.Text = "D66C7D32";
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(784, 483);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.txtOdszyfrowany);
			this.Controls.Add(this.txtZaszyfrowany);
			this.Controls.Add(this.txtNieszyfrowany);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmMain";
			this.Text = "Kodowanie Stringów";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudK)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtZaszyfrowany;
		private System.Windows.Forms.TextBox txtOdszyfrowany;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton rdoCezar;
		private System.Windows.Forms.RadioButton rdoMacierz;
		private System.Windows.Forms.RadioButton rdoRail;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown nudN;
		private System.Windows.Forms.Button cmdSzyfruj;
		private System.Windows.Forms.TextBox txtKlucz;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.NumericUpDown nudK;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button cmdOdszyfruj;
		public System.Windows.Forms.TextBox txtNieszyfrowany;
		private System.Windows.Forms.RadioButton rdoTDES;
		private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
		private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtTDESkey1;
		private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape4;
		private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape3;
		private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox txtTDESkey2;
	}
}

