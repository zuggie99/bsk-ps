﻿using System;
using System.Windows.Forms;
using CryptoAlgorytmy;

namespace Crypto1Stringi
{
	public partial class frmMain : Form
	{
		public frmMain()
		{
			InitializeComponent();
		}

		private void cmdSzyfruj_Click(object sender, EventArgs e)
		{
			if (rdoRail.Checked)
			{
				txtZaszyfrowany.Text = RailFence.Zaszyfruj(txtNieszyfrowany.Text, (int) nudN.Value);
			}
			else if (rdoMacierz.Checked)
			{
				txtZaszyfrowany.Text = KryptosystemPrzestawieniowy.Zaszyfruj(txtNieszyfrowany.Text, txtKlucz.Text);
			}
			else if (rdoCezar.Checked)
			{
				txtZaszyfrowany.Text = SzyfrCezara.Zaszyfruj(txtNieszyfrowany.Text, (int) nudK.Value);
			}
			else if (rdoTDES.Checked)
			{
				txtZaszyfrowany.Text = TDES.Zaszyfruj(txtNieszyfrowany.Text, txtTDESkey1.Text, txtTDESkey2.Text);
			}
		}

		private void cmdOdszyfruj_Click(object sender, EventArgs e)
		{
			if (rdoRail.Checked)
			{
				txtOdszyfrowany.Text = RailFence.Odszyfruj(txtZaszyfrowany.Text, (int) nudN.Value);
			}
			else if (rdoMacierz.Checked)
			{
				txtOdszyfrowany.Text = KryptosystemPrzestawieniowy.Odszyfruj(txtZaszyfrowany.Text, txtKlucz.Text);
			}
			else if (rdoCezar.Checked)
			{
				txtOdszyfrowany.Text = SzyfrCezara.Odszyfruj(txtZaszyfrowany.Text, (int) nudK.Value);
			}
			else if (rdoTDES.Checked)
			{
				txtOdszyfrowany.Text = TDES.Odszyfruj(txtZaszyfrowany.Text, txtTDESkey1.Text, txtTDESkey2.Text);
			}
		}
	}
}
