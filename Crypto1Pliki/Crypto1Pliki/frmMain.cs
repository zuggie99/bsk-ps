﻿using System;
using System.IO;
using System.Windows.Forms;
using CryptoAlgorytmy;

// namespace: Crypto1Pliki
//
// summary:	.

namespace Crypto1Pliki
{
	/// <summary>	A form main. </summary>
	///
	/// <remarks>	Adam, 3/28/2014. </remarks>

	public partial class frmMain : Form
	{
		String[] pliki = null;

		public frmMain()
		{
			InitializeComponent();
		}

		private void cmdSource_Click(object sender, EventArgs e)
		{
			DialogResult r = ofdPliki.ShowDialog();
			if (r == System.Windows.Forms.DialogResult.OK)
			{
				pliki = ofdPliki.FileNames;
			}
		}
		
		private void cmdSzyfruj_Click(object sender, EventArgs e)
		{
			if (pliki == null) return;

			foreach (String file in pliki)
			{
				String tekst = File.ReadAllText(file);
				String szyfr = String.Empty;

				if (rdoRail.Checked)
				{
					szyfr = RailFence.Zaszyfruj(tekst, (int) nudN.Value);
				}
				else if (rdoMacierz.Checked)
				{
					szyfr = KryptosystemPrzestawieniowy.Zaszyfruj(tekst, txtKlucz.Text);
				}
				else if (rdoCezar.Checked)
				{
					szyfr = SzyfrCezara.Zaszyfruj(tekst, (int) nudK.Value);
				}

				String newfile = file.Insert(file.LastIndexOf("\\") + 1, txtPrefix.Text.Trim());
				txtSource.Text += "\"" + file + "\" zakodowana jako \"" + newfile + "\"";

				File.WriteAllText(newfile, szyfr);
			}

			pliki = null;
		}

		private void cmdOdszyfruj_Click(object sender, EventArgs e)
		{
			if (pliki == null) return;

			foreach (String file in pliki)
			{
				String tekst = File.ReadAllText(file);
				String szyfr = String.Empty;

				if (rdoRail.Checked)
				{
					szyfr = RailFence.Odszyfruj(tekst, (int) nudN.Value);
				}
				else if (rdoMacierz.Checked)
				{
					szyfr = KryptosystemPrzestawieniowy.Odszyfruj(tekst, txtKlucz.Text);
				}
				else if (rdoCezar.Checked)
				{
					szyfr = SzyfrCezara.Odszyfruj(tekst, (int) nudK.Value);
				}

				String newfile = file.Insert(file.LastIndexOf("\\") + 1, txtPrefix.Text.Trim());
				txtSource.Text += "\"" + file + "\" odkodowana jako \"" + newfile + "\"";

				File.WriteAllText(newfile, szyfr);
			}

			pliki = null;
		}
	}
}
