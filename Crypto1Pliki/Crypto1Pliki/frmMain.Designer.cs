﻿namespace Crypto1Pliki
{
	partial class frmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ofdPliki = new System.Windows.Forms.OpenFileDialog();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.cmdOdszyfruj = new System.Windows.Forms.Button();
			this.cmdSzyfruj = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.nudK = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.txtKlucz = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.nudN = new System.Windows.Forms.NumericUpDown();
			this.rdoCezar = new System.Windows.Forms.RadioButton();
			this.rdoMacierz = new System.Windows.Forms.RadioButton();
			this.rdoRail = new System.Windows.Forms.RadioButton();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtPrefix = new System.Windows.Forms.TextBox();
			this.cmdSource = new System.Windows.Forms.Button();
			this.txtSource = new System.Windows.Forms.TextBox();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudN)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// ofdPliki
			// 
			this.ofdPliki.Filter = "All files|*.*";
			this.ofdPliki.Multiselect = true;
			this.ofdPliki.SupportMultiDottedExtensions = true;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.cmdOdszyfruj);
			this.groupBox3.Controls.Add(this.cmdSzyfruj);
			this.groupBox3.Controls.Add(this.label6);
			this.groupBox3.Controls.Add(this.nudK);
			this.groupBox3.Controls.Add(this.label5);
			this.groupBox3.Controls.Add(this.txtKlucz);
			this.groupBox3.Controls.Add(this.label4);
			this.groupBox3.Controls.Add(this.nudN);
			this.groupBox3.Controls.Add(this.rdoCezar);
			this.groupBox3.Controls.Add(this.rdoMacierz);
			this.groupBox3.Controls.Add(this.rdoRail);
			this.groupBox3.Location = new System.Drawing.Point(12, 12);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(680, 94);
			this.groupBox3.TabIndex = 4;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Typ szyfrowania";
			// 
			// cmdOdszyfruj
			// 
			this.cmdOdszyfruj.Location = new System.Drawing.Point(503, 56);
			this.cmdOdszyfruj.Name = "cmdOdszyfruj";
			this.cmdOdszyfruj.Size = new System.Drawing.Size(138, 23);
			this.cmdOdszyfruj.TabIndex = 15;
			this.cmdOdszyfruj.Text = "Odszyfruj";
			this.cmdOdszyfruj.UseVisualStyleBackColor = true;
			this.cmdOdszyfruj.Click += new System.EventHandler(this.cmdOdszyfruj_Click);
			// 
			// cmdSzyfruj
			// 
			this.cmdSzyfruj.Location = new System.Drawing.Point(503, 20);
			this.cmdSzyfruj.Name = "cmdSzyfruj";
			this.cmdSzyfruj.Size = new System.Drawing.Size(138, 23);
			this.cmdSzyfruj.TabIndex = 7;
			this.cmdSzyfruj.Text = "Zaszyfruj";
			this.cmdSzyfruj.UseVisualStyleBackColor = true;
			this.cmdSzyfruj.Click += new System.EventHandler(this.cmdSzyfruj_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(310, 66);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(48, 13);
			this.label6.TabIndex = 14;
			this.label6.Text = "k - offset";
			// 
			// nudK
			// 
			this.nudK.Location = new System.Drawing.Point(313, 42);
			this.nudK.Name = "nudK";
			this.nudK.Size = new System.Drawing.Size(54, 20);
			this.nudK.TabIndex = 13;
			this.nudK.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(158, 66);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(33, 13);
			this.label5.TabIndex = 12;
			this.label5.Text = "Klucz";
			// 
			// txtKlucz
			// 
			this.txtKlucz.Location = new System.Drawing.Point(161, 42);
			this.txtKlucz.Name = "txtKlucz";
			this.txtKlucz.Size = new System.Drawing.Size(95, 20);
			this.txtKlucz.TabIndex = 11;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(19, 66);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(73, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "n - głębokość";
			// 
			// nudN
			// 
			this.nudN.Location = new System.Drawing.Point(19, 43);
			this.nudN.Name = "nudN";
			this.nudN.Size = new System.Drawing.Size(54, 20);
			this.nudN.TabIndex = 9;
			this.nudN.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
			// 
			// rdoCezar
			// 
			this.rdoCezar.AutoSize = true;
			this.rdoCezar.Location = new System.Drawing.Point(312, 19);
			this.rdoCezar.Name = "rdoCezar";
			this.rdoCezar.Size = new System.Drawing.Size(58, 17);
			this.rdoCezar.TabIndex = 2;
			this.rdoCezar.Text = "Cezara";
			this.rdoCezar.UseVisualStyleBackColor = true;
			// 
			// rdoMacierz
			// 
			this.rdoMacierz.AutoSize = true;
			this.rdoMacierz.Location = new System.Drawing.Point(161, 20);
			this.rdoMacierz.Name = "rdoMacierz";
			this.rdoMacierz.Size = new System.Drawing.Size(82, 17);
			this.rdoMacierz.TabIndex = 1;
			this.rdoMacierz.Text = "Macierzowe";
			this.rdoMacierz.UseVisualStyleBackColor = true;
			// 
			// rdoRail
			// 
			this.rdoRail.AutoSize = true;
			this.rdoRail.Checked = true;
			this.rdoRail.Location = new System.Drawing.Point(19, 20);
			this.rdoRail.Name = "rdoRail";
			this.rdoRail.Size = new System.Drawing.Size(73, 17);
			this.rdoRail.TabIndex = 0;
			this.rdoRail.TabStop = true;
			this.rdoRail.Text = "Rail fence";
			this.rdoRail.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.txtPrefix);
			this.groupBox1.Controls.Add(this.cmdSource);
			this.groupBox1.Location = new System.Drawing.Point(13, 112);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(680, 50);
			this.groupBox1.TabIndex = 5;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Pliki";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(208, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(97, 13);
			this.label1.TabIndex = 13;
			this.label1.Text = "Dodaj Przedrostek:";
			// 
			// txtPrefix
			// 
			this.txtPrefix.Location = new System.Drawing.Point(311, 19);
			this.txtPrefix.Name = "txtPrefix";
			this.txtPrefix.Size = new System.Drawing.Size(330, 20);
			this.txtPrefix.TabIndex = 12;
			// 
			// cmdSource
			// 
			this.cmdSource.Location = new System.Drawing.Point(21, 17);
			this.cmdSource.Name = "cmdSource";
			this.cmdSource.Size = new System.Drawing.Size(109, 23);
			this.cmdSource.TabIndex = 11;
			this.cmdSource.Text = "Wybierz ...";
			this.cmdSource.UseVisualStyleBackColor = true;
			this.cmdSource.Click += new System.EventHandler(this.cmdSource_Click);
			// 
			// txtSource
			// 
			this.txtSource.Location = new System.Drawing.Point(13, 168);
			this.txtSource.Multiline = true;
			this.txtSource.Name = "txtSource";
			this.txtSource.ReadOnly = true;
			this.txtSource.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtSource.Size = new System.Drawing.Size(681, 208);
			this.txtSource.TabIndex = 9;
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(705, 388);
			this.Controls.Add(this.txtSource);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.groupBox3);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmMain";
			this.Text = "Szyfrowanie plików";
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudN)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.OpenFileDialog ofdPliki;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button cmdOdszyfruj;
		private System.Windows.Forms.Button cmdSzyfruj;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.NumericUpDown nudK;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtKlucz;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown nudN;
		private System.Windows.Forms.RadioButton rdoCezar;
		private System.Windows.Forms.RadioButton rdoMacierz;
		private System.Windows.Forms.RadioButton rdoRail;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtPrefix;
		private System.Windows.Forms.Button cmdSource;
		private System.Windows.Forms.TextBox txtSource;
	}
}